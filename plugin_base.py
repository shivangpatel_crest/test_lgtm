from typing import List
from os.path import dirname, join
import base64
import json
import inspect

from .utils.logger import Logger
from .models import Log, LogType
from .models.indicator import Indicator


class PluginMount(type):
    def __init__(cls, name, base, attrs):
        if not hasattr(cls, "plugins"):
            cls.plugins = []
        else:
            if cls.load_metadata():  # add only if manifest is parsed successfully
                cls.plugins.append(cls)


class PluginBase(metaclass=PluginMount):
    def __init__(self, logger, storage):
        self._storage = storage
        self._logger = logger

    def pull(self) -> List[Indicator]:
        raise NotImplementedError()

    def push(self, indicators: List[Indicator]):
        raise NotImplementedError()

    def validate(self, configuration: dict):
        raise NotImplementedError()

    @classmethod
    def load_metadata(cls):
        """Load the metadata of a plugin.
        
        Returns:
            bool: Indicates if the metadata was loaded successfully or not
        """
        try:
            plugin_dir = dirname(inspect.getmodule(cls).__file__)
            if not hasattr(cls, "metadata"):  # if it has not already been loaded
                cls.metadata = json.load(  # load from the manifest.json file
                    open(join(plugin_dir, "manifest.json"))
                )
                # load the plugin
                with open(join(plugin_dir, "icon.png"), "rb") as icon_file:
                    icon = base64.b64encode(icon_file.read())
                    cls.metadata["icon"] = icon
            return True
        except Exception:
            logger = Logger()
            logger.error("Error while loading plugin. Could not parse manifest")
            return False

    @property
    def logger(self) -> Logger:
        """Logger object
        
        Returns:
            cte.utils.logger.Logger: Initialized logger object
        """
        return self._logger

    @property
    def storage(self) -> dict:
        """Persistent storage for the configurations.
        
        Returns:
            dict: Storage dictionary.
        """
        return self._storage
