from typing import List
from importlib import import_module
from os.path import dirname, join, sep
import glob

from cte.plugin_base import PluginBase
from cte.utils.singleton import Singleton
from cte.utils import Logger


class PluginHelper(metaclass=Singleton):
    def __init__(self, plugin_pkg):
        """Constructor
        
        Args:
            plugin_pkg (module): The module which contains all the plugin packages
        """
        self._pkg = plugin_pkg
        self._logger = Logger()
        self.refresh()  # first time loading

    def refresh(self):
        """Reloads all the plugins. Use if a plugin is changed or new plugin added. Should be hooked up with an API endpoint to reload plugins on demand.
        """
        # import all plugin packages
        for plugin in self._plugin_packages():
            try:
                import_module(plugin)
            except Exception:
                self._logger.error(f"Error occurred while importing plugin {plugin}")

    def _plugin_packages(self) -> List[str]:
        """Returns list of all the packages that contains main.py in the plugins dir.
        
        Returns:
            list: List of absolute package names.
        """
        # create a list of plugin package strings
        return [
            "cte.plugins." + ".".join(p.split(sep)[-2:])[:-3]
            for p in glob.glob(join(dirname(self._pkg.__file__), "*", "main.py"))
        ]

    def find_by_id(self, _id) -> PluginBase:
        """Return the plugin class by name.
        """
        for plugin in self.plugins:
            if plugin.metadata["id"] == _id:
                return plugin
        return None

    @property
    def plugins(self) -> List[PluginBase]:
        """Returns a list of all mounted plugins.
        
        Returns:
            list: List of all the mounted plugin classes.
        """
        return PluginBase.plugins
